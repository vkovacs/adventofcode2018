package hu.crs.adventofcode2018.day4;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReposeRecord {

    public static void main(String[] args) throws URISyntaxException, IOException {
        System.out.println(GuardObservationService.processObservations(readObservations()));
    }

    private static List<String> readObservations() throws URISyntaxException, IOException {
        return Files.readAllLines(Paths.get(ClassLoader.getSystemResource("hu/crs.adventofcode2018/day4/GuardObservations.txt").toURI()));
    }
}
