package hu.crs.adventofcode2018.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SingleGuardObservation {
    private int id;
    private int[] minutes;

    private SingleGuardObservation(int id) {
        this.id = id;
        this.minutes = new int[60];
    }

    static List<SingleGuardObservation> asGuardObservations(List<String> guardRecords) {
        Pattern idPattern = Pattern.compile("#(\\d*)");
        Matcher guardIdMatcher;

        List<SingleGuardObservation> singleGuardObservations = new ArrayList<>();

        boolean awake = true;
        int minuteFellAsleep = 0;
        SingleGuardObservation currentSingleGuardObservation = null;

        for (String entry : guardRecords) {
            guardIdMatcher = idPattern.matcher(entry);
            boolean isGuardBeginsShift = guardIdMatcher.find();

            if (isGuardBeginsShift) {
                String guardId = guardIdMatcher.group(1);
                currentSingleGuardObservation = newGuardObservation(guardId);
                singleGuardObservations.add(currentSingleGuardObservation);
            } else {
                String minute = entry.substring(15, 17);
                if (awake) {
                    minuteFellAsleep = Integer.parseInt(minute);
                    awake = false;
                } else {
                    if (currentSingleGuardObservation == null) {
                        throw new IllegalStateException("GuardOperation should have been created.");
                    }
                    int minuteAwoke = Integer.parseInt(minute);
                    awake = true;
                    recordSleepingMinutesForGuard(minuteFellAsleep, currentSingleGuardObservation, minuteAwoke);
                }
            }
        }
        return singleGuardObservations;
    }

    private static void recordSleepingMinutesForGuard(int minuteFellAsleep, SingleGuardObservation currentSingleGuardObservation, int minuteAwoke) {
        for (int i = minuteFellAsleep; i < minuteAwoke; i++) {
            currentSingleGuardObservation.getMinutes()[i] += 1;
        }
    }

    private static SingleGuardObservation newGuardObservation(String guardId) {
        SingleGuardObservation currentSingleGuardObservation;
        currentSingleGuardObservation = new SingleGuardObservation(Integer.parseInt(guardId));
        return currentSingleGuardObservation;
    }


    int getId() {
        return id;
    }

    int[] getMinutes() {
        return minutes;
    }

    @Override
    public String toString() {
        return "GuardGuardObservationrd{" +
                "id=" + id +
                "}\n\n";
    }
}
