package hu.crs.adventofcode2018.day4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GuardObservationService {

    static int processObservations(List<String> observations) {
        Collections.sort(observations);
        List<SingleGuardObservation> singleGuardObservations = SingleGuardObservation.asGuardObservations(observations);
        List<AccumulatedGuardObservation> accumulatedGuardObservations = groupByGuardId(singleGuardObservations);

        AccumulatedGuardObservation laziestAccumulatedGuardObservation = laziestGuardObservation(accumulatedGuardObservations);
        return laziestAccumulatedGuardObservation.getId() * laziestAccumulatedGuardObservation.getMinuteTheGuardSleepsTheMost();
    }

    private static List<AccumulatedGuardObservation> groupByGuardId(List<SingleGuardObservation> singleGuardObservations) {
        List<AccumulatedGuardObservation> accumulatedGuardObservations = new ArrayList<>();

        for (Integer guardId : distinctGuardIds(singleGuardObservations)) {
            int[] minutes = new int[60];
            for (SingleGuardObservation observation : guardSpecificObservations(singleGuardObservations, guardId)) {
                for (int i = 0; i < 60; i++) {
                    minutes[i] += observation.getMinutes()[i];
                }
            }
            accumulatedGuardObservations.add(new AccumulatedGuardObservation(guardId, minutes));
        }

        return accumulatedGuardObservations;
    }

    private static AccumulatedGuardObservation laziestGuardObservation(List<AccumulatedGuardObservation> accumulatedGuardObservations) {
        AccumulatedGuardObservation laziestAccumulatedGuardObservation = accumulatedGuardObservations.get(0);
        for (int i = 1; i < accumulatedGuardObservations.size(); i++) {
            if (accumulatedGuardObservations.get(i).getSumMinutesTheGuardSlept() > laziestAccumulatedGuardObservation.getSumMinutesTheGuardSlept()) {
                laziestAccumulatedGuardObservation = accumulatedGuardObservations.get(i);
            }
        }
        return laziestAccumulatedGuardObservation;
    }

    private static List<SingleGuardObservation> guardSpecificObservations(List<SingleGuardObservation> singleGuardObservations, Integer guardId) {
        return singleGuardObservations.stream()
                    .filter(o -> o.getId() == guardId)
                    .collect(Collectors.toList());
    }

    private static List<Integer> distinctGuardIds(List<SingleGuardObservation> singleGuardObservations) {
        return singleGuardObservations.stream()
                .map(SingleGuardObservation::getId)
                .distinct()
                .collect(Collectors.toList());
    }

}
