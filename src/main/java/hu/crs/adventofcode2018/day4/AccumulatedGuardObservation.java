package hu.crs.adventofcode2018.day4;

import java.util.Arrays;

public class AccumulatedGuardObservation {
    private int id;
    private int[] sumSleepMinutes;

    AccumulatedGuardObservation(int id, int[] sumSleepMinutes) {
        this.id = id;
        this.sumSleepMinutes = sumSleepMinutes;
    }

    int getId() {
        return id;
    }

    int getSumMinutesTheGuardSlept() {
        return Arrays.stream(sumSleepMinutes).sum();
    }

    int getMinuteTheGuardSleepsTheMost() {
        int maxIndex = 0;
        for (int i = 0; i < 60; i++) {
            if (sumSleepMinutes[i] > sumSleepMinutes[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    @Override
    public String toString() {
        return "AccumulatedGuardObservation{" +
                "id=" + id +
                ", sumSleepMinutes=" + Arrays.toString(sumSleepMinutes) +
                '}';
    }
}
