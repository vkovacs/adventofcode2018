package hu.crs.adventofcode2018.day1.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class FrequencyUtil {
    private FrequencyUtil() {
    }

    public static List<Integer> frequencyChanges(Path path) throws IOException {
        return Files.readAllLines(path)
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }
}
