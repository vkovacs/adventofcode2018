package hu.crs.adventofcode2018.day1;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static hu.crs.adventofcode2018.day1.util.FrequencyUtil.frequencyChanges;

public class FrequencyPartOne {

    public static void main(String[] args) throws IOException, URISyntaxException {
        Path frequencyChangesFile = Paths.get(ClassLoader.getSystemResource("hu/crs.adventofcode2018/day1/ChronalCalibration.txt").toURI());

        int sum = sum(frequencyChanges(frequencyChangesFile));

        System.out.println(sum);
    }

    static int sum(List<Integer> frequencyChanges) {
        return frequencyChanges.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
