package hu.crs.adventofcode2018.day1;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hu.crs.adventofcode2018.day1.util.FrequencyUtil.frequencyChanges;

public class FrequencyTwo {

    public static void main(String[] args) throws URISyntaxException, IOException {
        Path frequencyChangesFile = Paths.get(ClassLoader.getSystemResource("hu/crs.adventofcode2018/day1/ChronalCalibration.txt").toURI());

        int firstFrequencyReachedTwice = findFirstFrequencyReachedTwice(frequencyChanges(frequencyChangesFile));

        System.out.println(firstFrequencyReachedTwice);
    }

    static int findFirstFrequencyReachedTwice(List<Integer> frequencyChanges) {
        Map<Integer, Boolean> frequencyReached = new HashMap<>();

        int currentFrequency = 0;
        frequencyReached.put(0, Boolean.TRUE);

        while (true) {
            for (Integer frequencyChange : frequencyChanges) {
                currentFrequency += frequencyChange;

                if (frequencyReached.get(currentFrequency) == Boolean.TRUE) {
                    return currentFrequency;
                }

                frequencyReached.put(currentFrequency, Boolean.TRUE);
            }
        }
    }

}
