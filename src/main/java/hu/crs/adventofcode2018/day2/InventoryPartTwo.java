package hu.crs.adventofcode2018.day2;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

class InventoryPartTwo {

    public static void main(String[] args) throws IOException, URISyntaxException {
        Path inventoryFile = Paths.get(ClassLoader.getSystemResource("hu/crs.adventofcode2018/day2/Inventory.txt").toURI());
        List<String> inventory = Files.readAllLines(inventoryFile);


        for (String outerItem : inventory) {
            for (String innerItem : inventory) {
                String similarCharacters = charactersInSimilarPositions(outerItem, innerItem);

                if (hasOnlyOneDifferentCharacter(outerItem, similarCharacters)) {
                    return;
                }

            }
        }

    }

    private static boolean hasOnlyOneDifferentCharacter(String outerItem, String similarCharacters) {
        return similarCharacters.length() == outerItem.length() - 1;
    }

    static String charactersInSimilarPositions(String string0, String string1) {
        if (string0 == null || string1 == null || string0.length() != string1.length()) {
            throw new IllegalArgumentException();
        }

        if (string0.equals(string1)) {
            return string0;
        }

        final int commonStringLength = string0.length();

        StringBuilder similarlyPositionedCharacters = new StringBuilder();
        for (int i = 0; i < commonStringLength; i++) {
            if (string0.charAt(i) == string1.charAt(i)) {
                similarlyPositionedCharacters.append(string0.charAt(i));
            }
        }
        return similarlyPositionedCharacters.toString();
    }
}
