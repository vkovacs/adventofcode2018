package hu.crs.adventofcode2018.day2;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InventoryPartOne {

    private static final String EXACTLY_TWO = "exactlyTwo";
    private static final String EXACTLY_THREE = "exactlyThree";

    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> boxIds = Files.readAllLines(Paths.get(ClassLoader.getSystemResource("hu/crs.adventofcode2018/day2/Inventory.txt").toURI()));

        List<Map<String, Boolean>> boxIdLetterCounts = boxIds.stream()
                .map(InventoryPartOne::countLetters)
                .collect(Collectors.toList());

        long exactlyTwos = boxIdLetterCounts.stream()
                .filter(m -> m.get(EXACTLY_TWO))
                .count();

        long exactlyThrees = boxIdLetterCounts.stream()
                .filter(e -> e.get(EXACTLY_THREE))
                .count();

        long checksum = checksum(exactlyTwos, exactlyThrees);

        System.out.println(checksum);
    }

    static Map<String, Boolean> countLetters(String boxId) {

        Map<String, Long> charactersCount = Stream.of(boxId.split("")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        long occursExactlyTwiceCount = charactersCount.values().stream()
                .filter(v -> v == 2L)
                .count();
        long occursExactlyThreeTimesCount = charactersCount.values().stream()
                .filter(v -> v == 3L)
                .count();

        return Map.of(EXACTLY_TWO, occursExactlyTwiceCount > 0, EXACTLY_THREE, occursExactlyThreeTimesCount > 0);
    }

    static long checksum(long exactlyTwos, long exactlyThrees) {
        return exactlyTwos * exactlyThrees;
    }
}
