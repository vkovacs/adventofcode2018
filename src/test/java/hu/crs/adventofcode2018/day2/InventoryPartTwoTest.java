package hu.crs.adventofcode2018.day2;

import org.junit.Test;

import static hu.crs.adventofcode2018.day2.InventoryPartTwo.charactersInSimilarPositions;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class InventoryPartTwoTest {
    @Test(expected = IllegalArgumentException.class)
    public void shouldReturnCharactersAsStringInSimilarPositionThrowIllegalArgumentExceptionForNullInput() {
        charactersInSimilarPositions(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldReturnCharactersAsStringInSimilarPositionThrowIllegalArgumentExceptionDifferentLengthInput() {
        charactersInSimilarPositions("a", "bc");
    }

    @Test
    public void shouldReturnCharactersOnSimilarPositionOneLengthSimilarStrings() {
        assertThat(charactersInSimilarPositions("a", "a"), is("a"));
    }

    @Test
    public void shouldReturnCharactersOnSimilarPositionOneLengthDifferentStrings() {
        assertThat(charactersInSimilarPositions("a", "b"), is(""));
    }

    @Test
    public void shouldReturnCharactersOnSimilarPositionMultipleLengthSimilarStrings() {
        assertThat(charactersInSimilarPositions("ab", "ab"), is("ab"));
    }

    @Test
    public void shouldReturnCharactersOnSimilarPositionMultipleLengthDifferentStrings() {
        assertThat(charactersInSimilarPositions("ab", "ba"), is(""));
    }

    @Test
    public void shouldReturnCharactersOnSimilarPositionMultipleLengthHasSomeSimilarCharactersStrings() {
        assertThat(charactersInSimilarPositions("abc", "adc"), is("ac"));
    }
}
