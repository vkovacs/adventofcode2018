package hu.crs.adventofcode2018.day2;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Map;

import static hu.crs.adventofcode2018.day2.InventoryPartOne.countLetters;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class InventoryPartOneTest {
    @Test
    public void shouldReturnNoTwoAndNoThreeOfAnyLetter() {
        assertThat(countLetters("abcdef"), equalTo(Map.of("exactlyTwo", FALSE, "exactlyThree", FALSE)));
    }

    @Test
    public void shouldReturnTwoOfELetter() {
        assertThat(countLetters("abcdee"), equalTo(Map.of("exactlyTwo", TRUE, "exactlyThree", FALSE)));
    }

    @Test
    public void shouldReturnCorrectCount() {
        assertThat(countLetters("bababc"), equalTo(Map.of("exactlyTwo", TRUE, "exactlyThree", TRUE)));
        assertThat(countLetters("bababc"), equalTo(Map.of("exactlyTwo", TRUE, "exactlyThree", TRUE)));
        assertThat(countLetters("abbcde"), equalTo(Map.of("exactlyTwo", TRUE, "exactlyThree", FALSE)));
        assertThat(countLetters("abcccd"), equalTo(Map.of("exactlyTwo", FALSE, "exactlyThree", TRUE)));
        assertThat(countLetters("aabcdd"), equalTo(Map.of("exactlyTwo", TRUE, "exactlyThree", FALSE)));
        assertThat(countLetters("ababab"), equalTo(Map.of("exactlyTwo", FALSE, "exactlyThree", TRUE)));
    }

    @Test
    public void shouldGenerateValidChecksum() {
        assertThat(InventoryPartOne.checksum(4, 3), Matchers.is(12L));
    }
}
