package hu.crs.adventofcode2018.day1;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class FrequencyPartOneTest {

    @Test
    public void shouldReturnSum() throws IOException, URISyntaxException {
        Assert.assertThat(FrequencyPartOne.sum(List.of(+1, +1, +1)), Matchers.is(3));
        Assert.assertThat(FrequencyPartOne.sum(List.of(+1, +1, -2)), Matchers.is(0));
        Assert.assertThat(FrequencyPartOne.sum(List.of(-1, -2, -3)), Matchers.is(-6));
    }
}
