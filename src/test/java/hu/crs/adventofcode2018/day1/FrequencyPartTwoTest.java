package hu.crs.adventofcode2018.day1;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FrequencyPartTwoTest {

    @Test
    public void shouldReturnFrequencyReachedTwice() {
        Assert.assertThat(FrequencyTwo.findFirstFrequencyReachedTwice(List.of(+1, -1)), Matchers.is(0));
        Assert.assertThat(FrequencyTwo.findFirstFrequencyReachedTwice(List.of(+3, +3, +4, -2, -4)), Matchers.is(10));
        Assert.assertThat(FrequencyTwo.findFirstFrequencyReachedTwice(List.of(-6, +3, +8, +5, -6)), Matchers.is(5));
        Assert.assertThat(FrequencyTwo.findFirstFrequencyReachedTwice(List.of(+7, +7, -2, -7, -4)), Matchers.is(14));
    }
}